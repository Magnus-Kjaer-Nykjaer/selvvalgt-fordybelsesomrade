[![pipeline status](https://gitlab.com/Magnus-Kjaer-Nykjaer/selvvalgt-fordybelsesomrade/badges/main/pipeline.svg)](https://gitlab.com/Magnus-Kjaer-Nykjaer/selvvalgt-fordybelsesomrade/-/commits/main)

# Velkommen til min selvvalgt fordybelsesområde side

For at komme ind på denne side kan du bruge dette link:
<a href="https://magnus-kjaer-nykjaer.gitlab.io/selvvalgt-fordybelsesomrade/">Selvvalgt fordybelsesområde</a>

