
Da slutbrugeren ikke har indflydelse på i hvilket land virksomheden ligger i og hvor meget denne virksomheden fokusere på it-sikkerhed. Så ud over at slutbrugeren kan lave sin due diligence omkring hvor de køber deres enheder fra, har de ikke så meget indflydelse på produktet og virksomheden.

Alt efter hvor i verden man køber sine produkter fra er der sat vise standarder og love op omkring hvordan virksomheder skal producere deres IoT enheder, når det kommer til it-sikkerhed.

Jeg har derfor fundet lidt frem omkring nogle af de regler og standarder der er omkring produktionene af IoT enheder.

## Regler og standarder producenten skal opfylde, angående IoT enheder

I kapitel II "ERHVERVSDRIVENDES FORPLIGTELSER" Artikel 10 i "EUROPA-PARLAMENTETS OG RÅDETS FORORDNING om horisontale cybersikkerhedskrav til produkter med digitale elementer og om ændring af forordning", snakkes der om de forpligtigelser som erhvervsdrivende har når det kommer til internet forbundet enheder, så som IoT enheder. 
Dette er bare et af kapitlerne i et langt dokument der handler om de regler og forpligtigelser som produkter skal leve op til når det kommer til it-sikkerhed.

Ud over at EU komisonen har lagt nogle love og regler frem for IoT enheder så findes der mange standarder som IoT enheder skal leve op til. 
Et eksempel kunne være ETSIs som har standarden, EN 303 645, som handler om IoT enheder og snakker om de baseline krav som en IoT enhed skal leve op til når det handler om it-sikkerhed. 

Ud over det er der også organisationer så som mitre som kommer med teknologier og arkitekture til produktion af sikre IoT enheder som virksomheder kan gøre brug af.

## Gode skikke for virksomheder at følge

Ud over de regler og standarder der allerede eksistere og som nok ikke altid bliver opretholdt er der også gode skikke som altid er smart for en virksomhed at følge i deres design process.

Nogle skikke som altid er dejligt at se i en virksomhed er hvis de følger secure by design og secure by default principperne, da det helt fjerne grunden til at slutbrugere skal tænke meget over it-sikkerhed da virksomheden allerede har tænkt over det og går efter at lave deres enheder så sikre som muligt.

### Kilder

Bilag 1 i Cyber Resilience Act

<https://eur-lex.europa.eu/legal-content/DA/TXT/HTML/?uri=CELEX:52022PC0454>

<https://www.mitre.org/sites/default/files/2021-11/prs-17-3086-5-network-enabled-security-embedded-IoT-systems-NESES.pdf>

<https://www.google.com/search?q=etsi+en+303+645&oq=etsi+en+303+645&gs_lcrp=EgZjaHJvbWUyBggAEEUYOdIBCDM5NDlqMGoxqAIAsAIA&sourceid=chrome&ie=UTF-8>

<https://www.etsi.org/deliver/etsi_en/303600_303699/303645/02.01.01_60/en_303645v020101p.pdf>

<https://www.cisa.gov/securebydesign>