
### Side note / disclaimer  

Dette er en list jeg har lavet over de top fem bedste ting man kan gøre for at gøre sine IoT enheder sikrere mod inficering af malware. Man kan aldrig være 100 procent sikker på at en enheder ikke kan inficeres men der findes skridt man kan tage for at gøre det sværere at inficere dem.

## 1. Ændre default passwords

Der stødes ofte ind i at IoT enheder gør brug af svage, gæt bar eller hardcoded passwords, hvilket gør det nemt for en trussel aktør at bryde ind i den givne enhed. Ud over det ser man ofte at folk aldrig får ændret default login credentialsene på deres enheder fra fabrikkens default credentials og det er derfor nemt for en trussels aktør at logge ind i IoT enheden ud at skulle ændre noget på enheden.

Derfor er det altid bedst at ændre passwordsne på sine enheder fra det de er kommet med til noget du selv har sat og som er sikkert og ikke nemt kan brydes. Dette kunne gøres ved at følge billedet under.

Password sikkerhed i numre:

![Password sikkerhed](../images/PasswordSecurity.png "Password sikkerhed")

## 2. Sluk for funktioner der ikke bruges

Mange IoT enheder har muligheden for at tilgås via internettet. Men hvis du kun bruger dem i dit hjemmenetværk, skal du deaktivere fjernadgang. Ligeledes har smart højttalere Bluetooth-forbindelse, ud over Wi-Fi. Bruger du det ikke? så giver det lige så meget mening bare at slukke for det.

Smart-tv'er kommer nogle gange med en stemmestyring, men denne funktion bliver ofte ikke brugt selv i stemmestyrede husstande. Hvor smarte assistenter såsom Google Assistant, Siri eller Alexa bestemmer. Det lyder måske paranoid, men en aktiv mikrofon, hvis den er hacket, kan også bruges til at lytte på dine samtaler, for f.eks. at høre om du er hjemme og om man kan bryde ind uden forstyrrelse eller lignende ting.

Således handler deaktivering af funktioner om at blokere så mange af disse indgangsvinkler som muligt.

## 3. Hold dine enheder opdater til den nyeste version

Ligesom god normal it skik så gælder det også for IoT enheder, at man holder dem opdateret til den nyeste version af deres firmware. 

Meningen bag at holde sine enheder opdateret til den nyeste version, er fordi nye versioner ofte kommer med sikkerheds rettelser, så selve enheden er mere sikker end den var før opdateringen. 

Denne opdatering gælder for både din Wi-Fi router og dine IoT enheder, da din Wi-Fi router er den første indgangs vinkel til dine IoT enheder. 

## 4. Opret et separat netværk til IoT enheder

Separering af netværket, eller netværks segmentering er en smart ting at gøre, da det separerer dine IoT enheder fra at kunne snakke med dine mere sensitive enheder, såsom dine computere eller smartphones. 

Dette gøres for at sørge for at dine IoT-enheder ikke bliver en angrebsvinkel for en trussels aktør, ind på dit hoved netværk.

Denne separering af netværket kan gøres på flere måder, men ofte så har netværks routere en mulighed for at oprette et gæste netværk eller et helt nyt netværk, som man så kan sætte sine IoT enheder til at bruge.

## 5. Hvis muligt implementer multi faktor autentifikation  

Rigtig mange hjemme sider og andre applikation, gør brug af multifactor authentication (MFA), ofte to faktor authentication (2FA). Dette gøres fordi at et password ikke alt er sikkert nok eller kan helt undgås, derfor er det vigtigt at gøre brug af MFA hvis det er muligt da det gøre det sværre for en trussels aktør at tilgå dine enheder.

Det langt fra altid at MFA er muligt at gøre brug af så denne sikring er kun hvis det er muligt.


## Foranstaltninger producenten kan tage med deres IoT enheder

Når det kommer til IoT enheder så er det rent faktisk ikke slut brugeren som kan gøre mest for at sikre deres enheder. Det er nemlig producenten der har det største ansvar når det kommer til sikring af deres produkter. Derfor er det også vigtigt at slut brugeren tænker lidt over hvem det er de køber produkter fra, da virksomheder fra hvis lande kan lytte på slut brugerens netværk via deres IoT enhed. Ud over det kan virksomheder der laver IoT enheder have et ryg om at lave usikre enheder og platformer.

Jeg har derfor lavet et afsnit omkring de foranstaltninger producenter kan tage og de regler som producenter har angående IoT enheder. 

[Producentens IoT sikkerheds foranstaltninger](https://magnus-kjaer-nykjaer.gitlab.io/selvvalgt-fordybelsesomrade/Selvvalgt%20fordybelsesomr%C3%A5de/ForanstaltningerForProducenten/)

### Kilder

- <https://owasp.org/www-project-internet-of-things/>

- <https://www.computer.org/publications/tech-news/trends/7-actionable-tips-to-secure-your-smart-home-and-iot-devices>


