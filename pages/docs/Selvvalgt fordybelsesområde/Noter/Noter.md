## Brainstrom

### Emne brainstrom

IoT enheds sikkerhed

Hvordan sikre man sine IoT enheder mod IoT botnets.

Hvordan sørge man for at ens IoT enheder ikke spionere på ens netværk.


### Indholds brainstorm

Måske en tutorial omkring hvordan man nemt beskytter sine IoT enheder.


Laver selv deres egen IoT enhed:
<https://www.hydac.com/da-dk/>

ISO, CIS-18, OWASP Iot Security og MITRE, Har måske noget om emnet.

Kig i ASVS dokumentet, der er et afsnit om IoT

Strukturer emnerene efter mine læringsmål.

Kom med eksempler fra virkeligeheden (Ham (Hacker Giraffe) fra darknet diaries som hackede printere og printet pewdiepie reklarmer på dem.)

Cyber Resilience Act og andre standarder der holder  

<https://eur-lex.europa.eu/legal-content/DA/TXT/HTML/?uri=CELEX:52022PC0454>

## Links

Goat projekt fokuseret på IoT enheder
<https://github.com/OWASP/IoTGoat/>

IoT angrebs vinkler
<https://wiki.owasp.org/index.php/OWASP_Internet_of_Things_Project#tab=IoT_Attack_Surface_Areas>

