
## Hvad er en IoT enhed

IoT står for Internet of Things og er et ecosystem som indeholder en masse forskellige enheder der er forbundet over internettet. IoT enheder kan være alt muligt, lige fra en person med en hjerte implantat eller et bondegårds dyr med en biochip i eller en bil med indbygget sensorer, eller hvilken som helst anden naturlig eller menneske lavet objekt der kan tildelt en Ip adresse og kan sende data over et netværk.

![IoT ecosystem](../images/iota-iot_system.png "IoT ecosystem")

## Hvilke angrebs flader har IoT enheder 

OWASP lavede i 2018 en top ti over de mest hyppige angrebsflader, som der er stødt på, når det kommer til IoT enheder. Denne top ti kan man se på billedet under.

![OWASP IoT top 10](../images/OWASP-IoT-Top-10-2018-final.jpg "OWASP IoT top 10")

OWASP er også i gang med at lave et framework der hedder Internet of Things Security Verification Standard (ISVS) som skal bruges til at lave en standard for sikkerheds krav til IoT enheder.

## Hvordan beskytter man sine IoT enheder

Top fem måder at sikre dine IoT enheder jeg har lavet.

[Top fem måder at sikre dine IoT enheder](https://magnus-kjaer-nykjaer.gitlab.io/selvvalgt-fordybelsesomrade/Selvvalgt%20fordybelsesomr%C3%A5de/TopFemSikringsM%C3%A5derAfEnheder/)

## Hvad er et botnets 

Et botnet (kort for “robot network”) er en samling af malware inficerede maskiner, som styres af en enkelt trussel aktør, ofte kaldt en “bot-herde”. Hvert enkelt maskine i dette botnet/flok kalder man for en bot. 

Disse bots bliver styret af en trussels aktør fra et enkelt centralt sted, kaldt en Command and control(eller C2), grunden til at man gør brug af en C2 er fordi det gør det meget nemmer for en trussels aktør at koordinere og sende kommandoer til deres tusindvis af bots i deres botnet.

Da et botnet består af en masse forskelige inficeret maskiner som trussels aktøren har kontrol over, har trussels aktøren mulighed for at holde deres bots opdateret og ændre deres opførsel/angrebs måde uden store problemer. Dette gør det også muligt for trussels aktøre at leje deres botnets ud til andre trussels aktøre til at lave et angreb på et bestemt mål.

## Hvad bruges botnets til

Indfor botnet verden findes der flere forskelige typer af botnets, som bruges til forskelige ting.

### Email spam bots

Selv om man ikke længere ligger mærke til spam mails, da vores spam filtre er blevet rigtig gode, så bliver de stadig brugt da der er en lille margin af Emails der kommer igennem. Selv om man ikke ligge så meget mærke til dem mere er Email spam botnets stadig nogle af de største botnets man ser nu til dags. 

Et eksempel på et stort Email spam botnet man ser nu tildags er Cutwail botnetet, også kendt som Pushdo eller Pandex botnet. Dette botnet angriber Windows maskliner via den malware Trojan som hedder Pushdo som inficere via Emails. I 2009 mente man at Cutwail botnetet var på omkring 1,5 til 2 millioner inficeret maskiner og kunne derfor sende omkring 74 miliarder spam mails om dagen hvilket svare til 46,5 procent af alt globalt spam.

### DDoS botnets

DDoS botnets til modsætning Email spam botnets bruges til at angribe maskiner eller netværk, for at bringe dem ned så de ikke kan bruges længere. Dette gøres ved at sende så mange kald til målet at målet ikke længere kan følge med og derfor lukker ned. DDoS angreb gøres af mange grunde så som personlige eller politiske motivationer eller for at forlange penge for at stoppe angrebet.

På grund af at botnets kan bruges til så mange forskelige ting, har ejeren af botnetet nemt mulighed for at ændre angrebs typen af botnetet, til noget andet uden store problemer. Så hvis ejeren af Cutwail botnetet f.eks. valgte at ændre deres botnet til at lave DDoS angreb i stedet for at sende spam Emails, vil det ikke være svært for dem at gøre dette.

### Målrettede botnets

Ud over DDoS og Email spam botnets, bliver de også brugt til en masse forskelige angrebs vinkler ind i netværk eller til at lytte til information på et netværk. 

Et eksempel på et målrettet botnet kunne være et botnet som en trussels aktør har sat op på en masse kreditkortbehandlings maskiner rundt omkring i verden. Dette giver trussels aktøren mulighed for at lytte til og skrive ned alt den information på de kreditkort som maskinene behandler, og derfra tage den data og sende til trussels aktørens C2 og sælge denne kreditkort videre til brug af kriminelle.

Et andet eksempel kunne være en trussels aktør der få inficeret en maskine i en virksomheds netværk og via den kan pivot videre rundt i netværket og inficere flere maskiner.


## Eksempel på et ikke farligt botnet

Da en anonym sikkerheds forsker en dag sad og leget med Nmap Scripting Engine (NSE) fandt han ud af at der var en stor mængde åbne indlejret systemer, også kendt som en IoT enhed, på internettet. Mange af dem var Linux baseret og tilød adgang via enten tomme eller default login credentials. Dette brugte han så til at bygge en distribueret port scanner til scanning af alle IPv4 adresser på nettet. Disse scanninger inkluderet scanninger for de mest bruge porte, ICMP ping, reverse DNS og SYN scanninger. Denne data valgte han så at analysere for at få en estimering af IP adresse brug og ud over dette valgte han at offenligt gøre alt den data han fandt ud af via dette botnet.

Dette botnet blev kaldt for Carna botnettet og på animationen under kan man se alt internet aktivitet på alle IP adresser på hele internettet i løbet af en 24 timer cyklus.

![Carna botnet](../images/Carnabotnet_geovideo_lowres.gif)




### Kilder

<https://www.techtarget.com/iotagenda/definition/Internet-of-Things-IoT>

<https://owasp.org/www-project-iot-security-verification-standard/#>

<https://owasp.org/www-project-internet-of-things/>

<https://www.paloaltonetworks.com/cyberpedia/what-is-botnet>

<https://www.cyber.nj.gov/threat-center/threat-profiles/botnet-variants/cutwail>

<https://census2012.sourceforge.net/paper.html>

