---
hide:
  - footer
  - toc
---

# Selvvalgt fordybelsesområde info side

På dette website finder du alt information omkring mit selvvalgt fordybelsesområde 

## Emne: Hvordan sikrer man sine IoT enheder mod IoT botnets.

### Indhold

Inden for it-sikkerhed og DDoS angreb høre man ofte om botnets. Disse botnets nu til dags består ofte af en masse IoT enheder som er på nettet og står i folks hjem. På grund af at de kan tilgå nettet vil trussels aktør gerne have dem som en del af deres botnets og til at bruge til f.eks. at sende requests til hjemmesider uden ejerens viden i en del af et stort DDoS angreb.

Opstilte HV spørgsmål med info om: Hvad er en IoT enhed, Hvilke angrebs flader har IoT enheder, Hvordan beskytter man sine IoT enheder, Hvad er botnets.

### Læringsmål

De valgte læringsmål stammer fra de uddannelsens mål for læringsudbytte fra den nationale studieordning kapitel 1, se: [https://esdhweb.ucl.dk/D22-1980440.pdf](https://esdhweb.ucl.dk/D22-1980440.pdf) 

**Viden**

Den studerende har viden om...

- Gængse it-sikkerhedstrusler 
- Sikringsmekanismer som indgår i sikre systemer
- It-sikkerhedsprincipper til design af sikre systemer


**Færdigheder**

Den studerende kan ...

- Anvende, vurdere og formidle it-sikkerhedsstandarder ift. forretningsbehov.
- Vælge, begrunde og formidle velegnede it-sikkerhedstiltag ift. givne forretningsmæssige scenarier.
- Identificere og argumentere for velegnede valg af relevante mekanismer til at imødegå identificerede it-sikkerhedstrusler.

**Kompetencer**

Den studerende kan ...

- Med udgangspunkt i bl.a. gængse it-sikkerhedsstandarder håndtere udarbejdelse af målrettede it-sikkerhedspolitikker og -procedurer ift. forretningsbehov.
- Håndtere komplekse situationer indenfor professionen.
- Selvstændigt tilegne sig viden, færdigheder og kompetencer indenfor it-sikkerhed.

# Milepæls plan

Her angives deadlines (datoer) for hvilke milepæle der arbejdes efter i projektet.

| Deadline   | Milepæl                                    |
| :--------- | :----------------------------------------- |
| 2023-09-05 | Projekt beskrevet og planlagt              |
| 2023-09-12 | Opsætning af projektet, projektstyring     |
| 2023-09-19 | Research botnet                            |
| 2023-09-26 | Research hvordan botnet gør brug af IoT    |
| 2023-10-03 | Research hvordan botnet gør brug af IoT    |
| 2023-10-10 | Test på en IoT enhed (hvis muligt)         |
| 2023-10-24 | Lave en tutorial (hvis muligt)             |
| 2023-10-31 | Lave en tutorial (hvis muligt)             |
| 2023-11-07 | Korrekturlæs produktet igennem             |
| 2023-11-14 | Eksamensaflevering færdig                  |

# Link til gitlab projekt 

<a href="https://trello.com/invite/b/T6FpR6se/ATTIeae8e27affd1a73a584cc52dfbfb4219E5C189C9/selvvalgt-fordybelsesomrade">Trello board invite link</a>

<a href="https://trello.com/b/T6FpR6se/selvvalgt-fordybelsesomr%C3%A5de">Eksisterende Trello bruger</a>

<a href="https://gitlab.com/Magnus-Kjaer-Nykjaer/selvvalgt-fordybelsesomrade">Repository link</a>

<a href="https://magnus-kjaer-nykjaer.gitlab.io/selvvalgt-fordybelsesomrade/">Pages link</a>

# Andet

- Måske en tutorial omkring hvordan man nemt beskytter sine IoT enheder.